import { Component } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public nome = "";
  public nomeado = false;
  private serverUrl = 'https://gamechain.herokuapp.com//socket';
  public title = 'Bem Vindo ao PlayerChat';
  private stompClient;

  constructor() {
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection() {
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/chat', (message) => {
        if (message.body) {
          $('.chat').append("<div class='message'>" + message.body + "</div>");
          console.log(message.body);
        }
      });
    });
  }

  sendMessage(message) {
    if (this.nomeado === false) {
      if (this.nome.length > 0) {
        this.nomeado = true;
        message = "Está online!";
      } else {
        alert("Nome obrigatorio");
      }
    }
    if (message === "") {
      alert("mensagem vazia");
      return;
    }
    this.stompClient.send("/app/send/message" , {}, this.nome + " - " + message);
    $('#input').val('');
  }

}
